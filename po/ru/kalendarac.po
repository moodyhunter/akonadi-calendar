# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the akonadi-calendar package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: akonadi-calendar\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-26 00:45+0000\n"
"PO-Revision-Date: 2022-09-26 17:48+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Александр Яворский"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kekcuha@gmail.com"

#: alarmnotification.cpp:71
#, kde-format
msgid "View"
msgstr "Вид"

#: alarmnotification.cpp:75
#, kde-format
msgid "Task"
msgstr "Задача"

#: alarmnotification.cpp:75
#, kde-format
msgid "Event"
msgstr "Событие"

#: alarmnotification.cpp:78
#, kde-format
msgid "Task due at %1"
msgstr "Срок выполнения: до %1"

#: alarmnotification.cpp:82
#, kde-format
msgctxt "Event starts in 5 minutes"
msgid "%2 starts in %1 minute"
msgid_plural "%2 starts in %1 minutes"
msgstr[0] "%2 начинается через %1 минуту"
msgstr[1] "%2 начинается через %1 минуты"
msgstr[2] "%2 начинается через %1 минут"
msgstr[3] "%2 начинается через %1 минуту"

#: alarmnotification.cpp:86
#, kde-format
msgctxt "Event starts at 10:00"
msgid "%1 starts at %2"
msgstr "%1 начинается в %2"

#: alarmnotification.cpp:88
#, kde-format
msgctxt "Event started at 10:00"
msgid "%1 started at %2"
msgstr "%1 начато в %2"

#: alarmnotification.cpp:94
#, kde-format
msgctxt "Event starts on <DATE> at <TIME>"
msgid "%1 starts on %2 at %3"
msgstr "%1 начинается %2 в %3"

#: alarmnotification.cpp:100
#, kde-format
msgctxt "Event started on <DATE> at <TIME>"
msgid "%1 started on %2 at %3"
msgstr "%1 начато %2 в %3"

#: alarmnotification.cpp:109
#, kde-format
msgctxt "Event starts on <DATE>"
msgid "%1 starts on %2"
msgstr "%1 начинается %2"

#: alarmnotification.cpp:111
#, kde-format
msgctxt "Event started on <DATE>"
msgid "%1 started on %2"
msgstr "%1 начато %2"

#: alarmnotification.cpp:131
#, kde-format
msgid "Remind in 5 mins"
msgstr "Напомнить через 5 минут"

#: alarmnotification.cpp:131
#, kde-format
msgid "Remind in 1 hour"
msgstr "Напомнить через один час"

#: alarmnotification.cpp:131
#, kde-format
msgctxt "dismiss a reminder notification for an event"
msgid "Dismiss"
msgstr "Отклонить"

#: alarmnotification.cpp:202
#, kde-format
msgid "Open URL"
msgstr "Открыть URL"

#: alarmnotification.cpp:220
#, kde-format
msgid "Map"
msgstr "Карта"

#: kalendaracmain.cpp:26
#, kde-format
msgctxt "@title"
msgid "Reminders"
msgstr "Напоминания"

#: kalendaracmain.cpp:29
#, kde-format
msgid "Calendar Reminder Service"
msgstr "Служба напоминания календаря"

#: kalendaracmain.cpp:33
#, kde-format
msgid "(c) KDE Community 2021-2022"
msgstr "© Сообщество KDE, 2021-2022"

#: kalendaracmain.cpp:34
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: kalendaracmain.cpp:35 kalendaracmain.cpp:39
#, kde-format
msgctxt "@info:credit"
msgid "Maintainer"
msgstr "Сопровождающий"

#: kalendaracmain.cpp:38
#, kde-format
msgctxt "@info:credit"
msgid "Clau Cambra"
msgstr "Clau Cambra"
